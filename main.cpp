#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sys/time.h>
#include <pthread.h>
#include <stdio.h>

using namespace std;

#define SIZE 16384
#define NUM_THREADS 16

float *myPlate;
float *otherPlate;
float *check;

float *newPlate;
float *oldPlate; 

bool loop;
bool balanced[NUM_THREADS];
int fifties[NUM_THREADS];
int iterations;

pthread_mutex_t logbarrier_count_lock;
pthread_barrier_t   barrier; // barrier synchronization object

void calculate(int startPosition, int endPosition) {
	for (int i = startPosition; i < endPosition; i++) {
        if (check[i]) {
            newPlate[i] = (oldPlate[i-1] + oldPlate[i+1] + oldPlate[i+SIZE] + oldPlate[i-SIZE] + 4 * oldPlate[i]) / 8.0;
        }
    }
}

bool steady(int startPosition, int endPosition, int thread_id) {
	bool steady = true;
	int fifty = 0;
	fifties[thread_id] = 0;
    for (int i = startPosition; i < endPosition; i++) {
        if (check[i]) {
	        if (fabs((newPlate[i-1] + newPlate[i+1] + newPlate[i+SIZE] + newPlate[i-SIZE])/ 4.0 - newPlate[i]) > 0.1) {
	            steady = false;
	        }
	        if (newPlate[i] > 50) {
        		fifties[thread_id] = fifty++;
	        }
	    }
    }
    return steady;
}

void copy() {
	float *temp = newPlate;
	newPlate = oldPlate;
	oldPlate = temp;
}


void *work(void* arg) {
	long thread_id = (long) arg;
	int eachThreadSize = SIZE * SIZE / NUM_THREADS;
	int startPosition = thread_id * eachThreadSize;
	int endPosition = startPosition + eachThreadSize;

	while (loop) {
		calculate(startPosition, endPosition);
		balanced[thread_id] = steady(startPosition, endPosition, thread_id);
		pthread_barrier_wait (&barrier);
		if (thread_id == 0) {
			bool keepGoing = false;
			int fifty = 0;
			for (int i = 0; i < NUM_THREADS; i++) {
				if (!balanced[i]) {
					keepGoing = true;
				}
				fifty += fifties[i];
				fifties[i] = 0;
			}
			loop = keepGoing;
			printf("%d\n", fifty);	
			fifty = 0;
			copy();
		}
		pthread_barrier_wait (&barrier);
	}
}

int main(int argc, char const *argv[])
{
	clock_t t1,t2;

	pthread_t threads[NUM_THREADS];
	pthread_barrier_init (&barrier, NULL, NUM_THREADS);
	pthread_mutex_init(&logbarrier_count_lock, NULL);

	iterations = 0;
	loop = true;

	myPlate = new float[SIZE * SIZE];
	otherPlate = new float[SIZE * SIZE];
	check = new float[SIZE * SIZE];

	newPlate = myPlate;
	oldPlate = otherPlate;

	//init entire plate
	for (int i = 0; i < SIZE; i++) {
		for (int j = 0; j < SIZE; j++) {
			if (i % 20 == 0) {
				myPlate[(i * SIZE) + j] = 100;
				otherPlate[(i * SIZE) + j] = 100;
				check[(i * SIZE) + j] = 0;
			} else if (j % 20 == 0) {
				myPlate[(i * SIZE) + j] = 0;
				otherPlate[(i * SIZE) + j] = 0;
				check[(i * SIZE) + j] = 0;
			} else {
				myPlate[(i * SIZE) + j] = 50;
				otherPlate[(i * SIZE) + j] = 50;
				check[(i * SIZE) + j] = 1;
			}
		}
	}

	//set left edge to 0
	for (int i = 0; i < SIZE; i++) {
		myPlate[(i * SIZE)] = 0;
		otherPlate[(i * SIZE)] = 0;

		check[(i * SIZE)] = 0;
	}

	//set right edge to 0
	for (int i = 0; i < SIZE; i++) {
		myPlate[(i * SIZE) + SIZE - 1] = 0;
		otherPlate[(i * SIZE) + SIZE - 1] = 0;

		check[(i * SIZE) + SIZE - 1] = 0;	
	}

	//set top edge to 0
	for (int i = 0; i < SIZE; i++) {
		myPlate[(SIZE * (SIZE - 1)) + i] = 0;
		otherPlate[(SIZE * (SIZE - 1)) + i] = 0;

		check[(SIZE * (SIZE - 1)) + i] = 0;
	}

	struct timeval start, end;
	gettimeofday(&start, NULL);

	for (long i = 0; i < NUM_THREADS; i++) {
		int rc = pthread_create(&threads[i], NULL, work, (void *) i);
	}

	for (long i = 0; i < NUM_THREADS; i++) {
		pthread_join(threads[i], NULL);
	}

	gettimeofday(&end, NULL);

	float delta = ((end.tv_sec  - start.tv_sec) * 1000000u + 
         end.tv_usec - start.tv_usec) / 1.e6;

	cout << "time = " << delta << endl;

	return 0;
}